import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {

        String example ="int c = 0; int a = 0; int b = 0; \n" +
                "Set set; \n" +
                "List list; \n" +
                "while (a < 5) { \n" +
                "   a = a + 1; \n" +
                "} \n" +
                "do { \n" +
                "   a = a - 1; \n" +
                "} while (a != 0); \n" +
                "if (a == 5) { \n" +
                "   b = 1; \n" +
                "} else { \n" +
                "   c = (1 + 2)/3  * (4 + 4)*2; \n" +
                "} \n" +
                "\n" +
                "set->add(1); \n" +
                "set->add(2); \n" +
                "set->add(3); \n" +
                "set->remove(2); \n" +
                "\n" +
                "list->add(5); \n" +
                "list->add(6); \n" +
                "list->clear();";

        List<Token> tokenList;
        List<Token> tokenStack;
        HashMap<String, String> variables;

        long startTime;
        long timeSpent;

        try {

            /*
             *  Lexer
             */

            System.out.println("Lexer output:");
            startTime = System.currentTimeMillis();
            tokenList = Lexer.parse(example);
            timeSpent = System.currentTimeMillis() - startTime;
            System.out.println(tokenList);

            System.out.println("Lexer working time: " + timeSpent + " ms");

            /*
             *  Parser
             */

            System.out.println();
            System.out.println("Parser output: ");
            startTime = System.currentTimeMillis();
            tokenStack = Parser.parse(tokenList);
            timeSpent = System.currentTimeMillis() - startTime;
            variables = Parser.getVariableTypes(tokenList);
            System.out.println(tokenStack);
            System.out.println(variables);

            System.out.println("Parser working time: " + timeSpent + " ms");

            /*
             *  Stack machine
             */

            System.out.println();
            System.out.println("Stack machine output: ");
            startTime = System.currentTimeMillis();
            StackMachine.execute(tokenStack, variables);
            timeSpent = System.currentTimeMillis() - startTime;
            System.out.println();
            System.out.println("Execution time: " + timeSpent + " ms");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
