import java.util.*;

public class StackMachine {
    private static HashMap<String, String> variableTypes;

    private static HashMap<String, Integer> integerValues = new HashMap<String, Integer>();
    private static HashMap<String, Boolean> booleanValues = new HashMap<String, Boolean>();
    private static HashMap<String, DumbHashSet> setValues = new  HashMap<String, DumbHashSet>();
    private static HashMap<String, DumbLinkedList> listValues = new  HashMap<String, DumbLinkedList>();

    private static Stack<Token> accumulator = new Stack<Token>();

    public static void execute(List<Token> tokenList, HashMap<String, String> variableTypes) throws Exception {

        ArrayList<Token> program = new ArrayList<Token>(tokenList);
        StackMachine.variableTypes = variableTypes;

        System.out.println("--- Execution process ---");

        for (int i = 0; i < program.size(); i++) {
            if(program.get(i) == null)
                break;

            System.out.print(i + "\t");
            System.out.print(program.get(i).getLexeme().getName() + ": ");
            System.out.println(program.get(i).getValue());

            if (program.get(i).getLexeme().equals(Lexeme.FALSE_TRANSITION))
                if (!Boolean.parseBoolean(accumulator.pop().getValue()))
                    i = Integer.parseInt(program.get(i).getValue());

            if (program.get(i).getLexeme().equals(Lexeme.TRUE_TRANSITION))
                if (Boolean.parseBoolean(accumulator.pop().getValue()))
                    i = Integer.parseInt(program.get(i).getValue());

            if (program.get(i).getLexeme().equals(Lexeme.UNCONDITIONAL_TRANSITION))
                i = Integer.parseInt(program.get(i).getValue());

            if (i >= program.size())
                break;

            if (program.get(i).getLexeme().equals(Lexeme.NUMBER)) {
                accumulator.push(program.get(i));

            } else if (program.get(i).getLexeme().equals(Lexeme.ID)) {
                accumulator.push(program.get(i));

            } else if (program.get(i).getLexeme().equals(Lexeme.TRUE)) {
                accumulator.push(program.get(i));

            } else if (program.get(i).getLexeme().equals(Lexeme.FALSE)) {
                accumulator.push(program.get(i));

            } else if (program.get(i).getLexeme().equals(Lexeme.ASSIGN)) {
                Token value = accumulator.pop();
                Token variable = accumulator.pop();

                if (variableTypes.get(variable.getValue()).equals("INTEGER")) {
                    integerValues.put(variable.getValue(), getNumber(value));

                } else if (variableTypes.get(variable.getValue()).equals("BOOLEAN")) {
                    integerValues.put(variable.getValue(), getNumber(value));

                } else {
                    throw new  IllegalArgumentException();

                }
            } else if (program.get(i).getLexeme().equals(Lexeme.PLUS)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Integer result = getNumber(firstValue) + getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            }  else if (program.get(i).getLexeme().equals(Lexeme.MINUS)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Integer result = getNumber(firstValue) - getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.MULTIPLY)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Integer result = getNumber(firstValue) * getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.DIVISION)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Integer result = getNumber(firstValue) / getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.MORE)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getNumber(firstValue) > getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.LESS)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getNumber(firstValue) < getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.LESS_EQUAL)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getNumber(firstValue) <= getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            }  else if (program.get(i).getLexeme().equals(Lexeme.MORE_EQUAL)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getNumber(firstValue) >= getNumber(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.EQUAL)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getNumber(firstValue).equals(getNumber(secondValue));
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.NOT_EQUAL)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = !getNumber(firstValue).equals(getNumber(secondValue));
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.AND)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getBoolean(firstValue) && getBoolean(secondValue);
                accumulator.push(new Token(result.toString()));

            } else if (program.get(i).getLexeme().equals(Lexeme.OR)) {
                Token secondValue = accumulator.pop();
                Token firstValue = accumulator.pop();

                Boolean result = getBoolean(firstValue) || getBoolean(secondValue);
                accumulator.push(new Token(result.toString()));

            }  else if (program.get(i).getLexeme().equals(Lexeme.ADD)) {
                Token value = accumulator.pop();
                Token name = accumulator.pop();

                String type = variableTypes.get(name.getValue());

                if (type.equals(Lexeme.SET.getName())) {
                    DumbHashSet set;

                    if (setValues.containsKey(name.getValue()))
                        set = setValues.get(name.getValue());
                    else
                        set = new DumbHashSet();

                    set.add(value.getValue());
                    setValues.put(name.getValue(), set);

                } else if (type.equals(Lexeme.LIST.getName())) {
                    DumbLinkedList list;

                    if (listValues.containsKey(name.getValue()))
                        list = listValues.get(name.getValue());
                    else
                        list = new DumbLinkedList();

                    list.add(value.getValue());
                    listValues.put(name.getValue(), list);

                } else
                    throw new IllegalArgumentException();

            } else if (program.get(i).getLexeme().equals(Lexeme.REMOVE)) {
                Token value = accumulator.pop();
                Token name = accumulator.pop();

                String type = variableTypes.get(name.getValue());

                DumbHashSet set;

                if (type.equals(Lexeme.SET.getName())) {
                    if (setValues.containsKey(name.getValue())) {
                        set = setValues.get(name.getValue());
                        set.remove(value.getValue());
                        setValues.put(name.getValue(), set);
                    } else
                        throw new Exception("No such element!");

                } else if (type.equals(Lexeme.LIST.getName())) {

                        DumbLinkedList list;

                        if (listValues.containsKey(name.getValue())) {
                            list = listValues.get(name.getValue());
                            list.remove(value.getValue());
                            listValues.put(name.getValue(), list);
                        } else
                            throw new Exception("No such element!");

                } else
                    throw new IllegalArgumentException();

            } else if (program.get(i).getLexeme().equals(Lexeme.CLEAR)) {
                Token name = accumulator.pop();

                String type = variableTypes.get(name.getValue());

                if (type.equals(Lexeme.SET.getName())) {
                    DumbHashSet set;

                    if (setValues.containsKey(name.getValue())) {
                        set = setValues.get(name.getValue());
                        set.clear();
                        setValues.put(name.getValue(), set);
                    } else
                        throw new Exception("No such element!");

                } else if (type.equals(Lexeme.LIST.getName())) {
                    DumbLinkedList list;

                    if (listValues.containsKey(name.getValue())) {
                        list = listValues.get(name.getValue());
                        list.clear();
                        listValues.put(name.getValue(), list);
                    } else
                        throw new Exception("No such element!");

                } else
                    throw new IllegalArgumentException();

            }
        }

        System.out.println();
        System.out.println("--- Execution results ---");
        System.out.println("Types of variables: " + variableTypes.toString());
        System.out.println("Integer variables: " + integerValues.toString());
        System.out.println("Boolean variables: " + booleanValues.toString());
        System.out.println("Set variables: " + setValues.toString());
        System.out.println("List variables: " + listValues.toString());
    }

    private static Integer getNumber(Token token) throws IllegalArgumentException {
        if (token.getLexeme().equals(Lexeme.NUMBER)) {
            return Integer.parseInt(token.getValue());

        } else if (token.getLexeme().equals(Lexeme.ID)) {
            if (variableTypes.get(token.getValue()).equals("INTEGER"))
                return integerValues.get(token.getValue());

        }

        throw new IllegalArgumentException();
    }

    private static Boolean getBoolean(Token token) throws IllegalArgumentException {
        if (token.getLexeme().equals(Lexeme.NUMBER)) {
            return Boolean.parseBoolean(token.getValue());

        } else if (token.getLexeme().equals(Lexeme.ID)) {
            if (variableTypes.get(token.getValue()).equals("BOOLEAN"))
                return booleanValues.get(token.getValue());

        }

        throw new IllegalArgumentException();
    }
}
