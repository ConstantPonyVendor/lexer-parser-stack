public class DumbLinkedList implements DumbCollection {
    private int size;
    private Node head;

    DumbLinkedList() {
        this.size = 0;
        this.head = null;
    }

    public class Node {

        private Object data;
        private Node next;

        void setData(Object data) {
            this.data = data;
        }
        void setNext(Node next) {
            this.next = next;
        }

        Object getData() {
            return this.data;
        }
        Node getNext() {
            return this.next;
        }
    }

    public int size() {
        return size;
    }

    public void add(Object data) {
        Node newNode = new Node();
        newNode.setData(data);

        if (head == null) {
            head = newNode;
            size++;
            return;
        }

        Node current = head;

        while(current != null) {
            if (current.getNext() == null) {
                current.setNext(newNode);
                size++;
                return;
            }

            current = current.getNext();
        }
    }

    public void remove(Object data) {
        if (head == null) { return; }

        if (head.getData().equals(data))
            head = head.getNext();

        Node current = head;
        Node previous = null;

        while(current != null) {
            if (current.getData().equals(data)) {
                assert previous != null;
                previous.setNext(current.getNext());
            }

            previous = current;
            current = current.getNext();
        }

        size--;
    }

    public Object get(int index) {
        if (head == null)
            return null;

        if (!(index < size))
            return null;

        Node current = head;

        while(current != null) {
            if (index == 0)
                return current.getData();

            current = current.getNext();
            index--;
        }
        return null;
    }

    public int indexOf(Object data) {
        int index = 0;
        Node current = head;

        while(current != null) {
            if (current.getData().equals(data))
                return index;

            current = current.getNext();
            index++;
        }

        return -1;
    }

    public void clear() {
        this.size = 0;
        this.head = null;
    }

    @Override
    public String toString() {
        Node current = head;
        StringBuilder sb = new StringBuilder();

        if (current == null)
            return "[]";

        while (current != null) {
            sb.append("[").append(current.getData()).append("]");

            if (current.getNext() != null)
                sb.append(" -> ");

            current = current.getNext();
        }

        return sb.toString();
    }
}
