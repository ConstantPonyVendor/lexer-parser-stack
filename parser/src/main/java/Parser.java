import java.io.IOException;
import java.util.*;

final class Parser {
    private static Stack<Token> tokenStack;
    private static HashMap<String, String> variableTypes;

    static List<Token> parse(List<Token> tokenList) throws IOException {
        Queue<Token> tokenQueue = new LinkedList<Token>(tokenList);

        tokenStack = new Stack<Token>();
        variableTypes = new HashMap<String, String>();

        for (Iterator<Token> iterator = tokenQueue.iterator(); iterator.hasNext(); ) {
            Token token = iterator.next();

            if (token.getLexeme() == Lexeme.WS)
                iterator.remove();
        }

        if (language(tokenQueue)) {
            return tokenStack;
        } else if (!tokenQueue.isEmpty())
            throw new IOException("Unexpected token \'" + tokenQueue.peek().getValue() +"\'!");
        else
            throw new IOException("Explicit syntax!");
    }

    static HashMap<String, String> getVariableTypes(List<Token> tokenList) throws IOException {
        parse(tokenList);
        return variableTypes;
    }

    private static boolean language(Queue<Token> tokenQueue) throws IOException {
        while (!tokenQueue.isEmpty())
            if (!statement(tokenQueue))
                return false;

        return true;
    }
    private static boolean statement(Queue<Token> tokenQueue) throws IOException {
        return  declaration(tokenQueue) || instruction(tokenQueue);
    }
    private static boolean instruction(Queue<Token> tokenQueue) throws IOException {
        return action(tokenQueue) ||
                loop(tokenQueue) ||
                condition_statement(tokenQueue) ||
                collection_edit(tokenQueue);
    }
    private static boolean declaration(Queue<Token> tokenQueue) throws IOException {
        Token accumulator;
        String name;
        String type;

        if (type(tokenQueue)) {
            type = tokenStack.pop().getLexeme().getName();

            if (ID(tokenQueue)) {
                name = tokenStack.peek().getValue();
                variableTypes.put(name, type);

                if (ASSIGN(tokenQueue)) {
                    accumulator = tokenStack.pop();

                    if (expression(tokenQueue)) {
                        tokenStack.push(accumulator);
                        return SEMICOLON(tokenQueue);
                    }
                } else
                    if  (SEMICOLON(tokenQueue)) {
                        tokenStack.pop();
                        return true;
                    }
            }
        }

        return false;
    }
    private static boolean condition_statement(Queue<Token> tokenQueue) throws IOException {
        Integer p1;
        Integer p2;
        Integer p3;

        if (IF(tokenQueue))
            if (bracket_bool(tokenQueue)) {
                p1 = tokenStack.size();

                if (block(tokenQueue)) {
                    p2 = tokenStack.size();

                    if (ELSE(tokenQueue)) {
                        if (block(tokenQueue)) {
                            p3 = tokenStack.size() + 2;
                            tokenStack.insertElementAt(new Token(Lexeme.UNCONDITIONAL_TRANSITION, p3.toString()), p2);
                            p2 += 2;
                            tokenStack.insertElementAt(new Token(Lexeme.FALSE_TRANSITION, p2.toString()), p1);
                            return true;
                        }
                    } else {
                        tokenStack.insertElementAt(new Token(Lexeme.FALSE_TRANSITION, p2.toString()), p1);
                        return true;
                    }
                }
            }

        return false;
    }
    private static boolean loop(Queue<Token> tokenQueue) throws IOException {
        Integer p1;
        Integer p2;
        Integer p3;

        if (WHILE(tokenQueue)) {
            p1 = tokenStack.size();

            if (bracket_bool(tokenQueue)) {
                p2 = tokenStack.size();

                if (block(tokenQueue)) {
                    p3 = tokenStack.size();
                    tokenStack.insertElementAt(new Token(Lexeme.UNCONDITIONAL_TRANSITION, p1.toString()), p3);
                    p3 += 2 ;
                    tokenStack.insertElementAt(new Token(Lexeme.FALSE_TRANSITION, p3.toString()), p2);
                    return true;
                }
            }
        }

        if (DO(tokenQueue)) {
            p1 = tokenStack.size();

            if (block(tokenQueue))
                if (WHILE(tokenQueue))
                    if (bracket_bool(tokenQueue)) {
                        p2 = tokenStack.size();
                        tokenStack.insertElementAt(new Token(Lexeme.TRUE_TRANSITION, p1.toString()), p2);
                        return SEMICOLON(tokenQueue);
                    }

        }

        return false;
    }
    private static boolean block(Queue<Token> tokenQueue) throws IOException {
        if (LEFT_CURLY_BRACKET(tokenQueue))
            while (statement(tokenQueue))
                if(RIGHT_CURLY_BRACKET(tokenQueue))
                    return true;

        return false;
    }

    private static boolean action(Queue<Token> tokenQueue) throws IOException {
        if (ID(tokenQueue))
           return  assign(tokenQueue) || collection_edit(tokenQueue);

        return false;
    }
    private static boolean assign(Queue<Token> tokenQueue) throws IOException {
        Token accumulator;

        if (ASSIGN(tokenQueue)) {
            accumulator = tokenStack.pop();

            if (expression(tokenQueue)) {
                tokenStack.push(accumulator);
                return SEMICOLON(tokenQueue);
            }
        }

        return false;
    }
    private static boolean bracket_expression(Queue<Token> tokenQueue) {
        if (LEFT_ROUND_BRACKET(tokenQueue))
            if(expression(tokenQueue))
                return RIGHT_ROUND_BRACKET(tokenQueue);

        return false;
    }
    private static boolean expression(Queue<Token> tokenQueue) {
        Token accumulator;

        if (term(tokenQueue) || bracket_expression(tokenQueue)) {
            while (PLUS(tokenQueue) || MINUS(tokenQueue)) {
                accumulator = tokenStack.pop();

                if (!term(tokenQueue) && !bracket_expression(tokenQueue))
                    return false;
                else
                    tokenStack.push(accumulator);
            }

            return true;
        }

        return false;
    }
    private static boolean term(Queue<Token> tokenQueue) {
        Token accumulator;

        if (unary(tokenQueue) || bracket_expression(tokenQueue)) {
            while (MULTIPLY(tokenQueue) || DIVISION(tokenQueue)) {
                accumulator = tokenStack.pop();

                if (!unary(tokenQueue) && !bracket_expression(tokenQueue))
                    return false;
                else
                    tokenStack.push(accumulator);
            }

            return true;
        }

        return false;
    }
    private static boolean unary(Queue<Token> tokenQueue) {
        if (NOT(tokenQueue) || MINUS(tokenQueue))
             if(factor(tokenQueue)) {
                tokenStack.push(tokenQueue.poll());
                return true;
             }

        return factor(tokenQueue);
    }
    private static boolean factor(Queue<Token> tokenQueue) {
        return ID(tokenQueue) || NUMBER(tokenQueue) || TRUE(tokenQueue) || FALSE(tokenQueue);
    }
    private static boolean type(Queue<Token> tokenQueue) {
        return BOOLEAN(tokenQueue) || INTEGER(tokenQueue) || SET(tokenQueue) || LIST(tokenQueue);
    }
    private static boolean bracket_bool(Queue<Token> tokenQueue) {
        if (LEFT_ROUND_BRACKET(tokenQueue))
            if(bool(tokenQueue))
                return RIGHT_ROUND_BRACKET(tokenQueue);

        return false;
    }
    private static boolean bool(Queue<Token> tokenQueue) {
        Token accumulator;

        if (join(tokenQueue) || bracket_bool(tokenQueue)) {
            while (OR(tokenQueue)) {
                accumulator = tokenStack.pop();

                if (!join(tokenQueue) && !bracket_bool(tokenQueue))
                    return false;
                else
                    tokenStack.push(accumulator);
            }

            return true;
        }

        return factor(tokenQueue);
    }
    private static boolean join(Queue<Token> tokenQueue) {
        Token accumulator;

        if (equality(tokenQueue) || bracket_bool(tokenQueue)) {
            while (AND(tokenQueue)) {
                accumulator = tokenStack.pop();

                if (!equality(tokenQueue) && !bracket_bool(tokenQueue))
                    return false;
                else
                    tokenStack.push(accumulator);
            }

            return true;
        }

        return false;
    }
    private static boolean equality(Queue<Token> tokenQueue) {
        Token accumulator;

        if (relation(tokenQueue))
            if (EQUAL(tokenQueue) || NOT_EQUAL(tokenQueue)) {
                accumulator = tokenStack.pop();

                if (relation(tokenQueue)) {
                    tokenStack.push(accumulator);
                    return true;
                }
            } else
                return true;

        return false;
    }
    private static boolean relation(Queue<Token> tokenQueue) {
        Token accumulator;

        if (expression(tokenQueue) || bracket_expression(tokenQueue))
            if (LESS(tokenQueue) || MORE(tokenQueue) || LESS_EQUAL(tokenQueue) || MORE_EQUAL(tokenQueue)) {
                accumulator = tokenStack.pop();

                if (expression(tokenQueue) || bracket_expression(tokenQueue)) {
                    tokenStack.push(accumulator);
                    return true;
                }
            } else
                return true;

        return false;
    }
    private static boolean collection_edit(Queue<Token> tokenQueue) throws IOException {
        List<Token> accumulator = new LinkedList<Token>();

            if(POINTER(tokenQueue)) {
                accumulator.add(tokenStack.pop());

                if (ADD(tokenQueue) || REMOVE(tokenQueue)) {
                    accumulator.add(tokenStack.pop());

                    if (LEFT_ROUND_BRACKET(tokenQueue))
                        if (NUMBER(tokenQueue))
                            if (RIGHT_ROUND_BRACKET(tokenQueue))
                                if (SEMICOLON(tokenQueue)) {
                                    tokenStack.addAll(accumulator);
                                    return true;
                                }
                } else if (CLEAR(tokenQueue)) {
                    accumulator.add(tokenStack.pop());

                    if (LEFT_ROUND_BRACKET(tokenQueue))
                        if (RIGHT_ROUND_BRACKET(tokenQueue))
                            if (SEMICOLON(tokenQueue)) {
                                tokenStack.addAll(accumulator);
                                return true;
                            }

                }
            }

        return false;
    }

    private static boolean ELSE(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.ELSE) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean NOT_EQUAL(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;


        if (tokenQueue.peek().getLexeme() == Lexeme.EQUAL) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean EQUAL(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.NOT_EQUAL) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean MORE_EQUAL(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.MORE_EQUAL) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }

    private static boolean LESS_EQUAL(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.LESS_EQUAL) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean MORE(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.MORE) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean LESS(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.LESS) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean AND(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.AND) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean OR(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.OR) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean INTEGER(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.INTEGER) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean BOOLEAN(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.BOOLEAN) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean LIST(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.LIST) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean SET(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.SET) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean FALSE(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.FALSE) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean TRUE(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.TRUE) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean NUMBER(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.NUMBER) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean NOT(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.NOT) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean MULTIPLY(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.MULTIPLY) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean DIVISION(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.DIVISION) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean MINUS(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.MINUS) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean PLUS(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.PLUS) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean SEMICOLON(Queue<Token> tokenQueue) throws IOException {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.SEMICOLON) {
            tokenQueue.poll();
            return true;
        }

        throw new IOException("Token \';\' expected!");
    }
    private static boolean IF(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.IF) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean DO(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.DO) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean RIGHT_ROUND_BRACKET(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.RIGHT_ROUND_BRACKET) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean LEFT_ROUND_BRACKET(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.LEFT_ROUND_BRACKET) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean RIGHT_CURLY_BRACKET(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.RIGHT_CURLY_BRACKET) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean LEFT_CURLY_BRACKET(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.LEFT_CURLY_BRACKET) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean WHILE(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.WHILE) {
            tokenQueue.poll();
            return true;
        }

        return false;
    }
    private static boolean ASSIGN(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.ASSIGN) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean ID(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.ID) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean POINTER(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.POINTER) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean ADD(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.ADD) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean REMOVE(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.REMOVE) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }
    private static boolean CLEAR(Queue<Token> tokenQueue) {
        if (tokenQueue.isEmpty())
            return false;

        if (tokenQueue.peek().getLexeme() == Lexeme.CLEAR) {
            tokenStack.push(tokenQueue.poll());
            return true;
        }

        return false;
    }

}


