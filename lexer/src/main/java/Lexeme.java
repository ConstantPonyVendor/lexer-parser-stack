import java.util.regex.*;

public enum Lexeme {

    WS("WS", Pattern.compile("^[ \t\n]+")),

    INTEGER("INTEGER", Pattern.compile("^int|INT")),
    BOOLEAN("BOOLEAN", Pattern.compile("^bool|boolen|BOOL|BOOLEAN")),
    SET("SET", Pattern.compile("^Set|SET")),
    LIST("LIST", Pattern.compile("^List|LIST")),

    PLUS("PLUS", Pattern.compile("^\\+")),
    MINUS("MINUS", Pattern.compile("^-")),
    MULTIPLY("MULTIPLY", Pattern.compile("^\\*")),
    DIVISION("DIVISION", Pattern.compile("^/")),

    MORE("MORE", Pattern.compile("^>")),
    LESS("LESS", Pattern.compile("^<")),
    MORE_EQUAL("MORE_EQUAL", Pattern.compile("^>=")),
    LESS_EQUAL("LESS_EQUAL", Pattern.compile("^<=")),
    EQUAL("EQUAL", Pattern.compile("^==")),
    NOT_EQUAL("NOT_EQUAL", Pattern.compile("^!=")),

    POINTER("POINTER", Pattern.compile("^->")),

    ADD("ADD", Pattern.compile("^add")),
    REMOVE("REMOVE", Pattern.compile("^remove")),
    CLEAR("CLEAR", Pattern.compile("^clear")),

    DO("DO", Pattern.compile("^do|DO")),
    WHILE("WHILE", Pattern.compile("^while|WHILE")),
    FOR("FOR", Pattern.compile("^for|FOR")),

    IF("IF", Pattern.compile("^if|IF")),
    ELSE("ELSE", Pattern.compile("^else|ELSE")),

    ASSIGN("ASSIGN", Pattern.compile("^=")),

    AND("AND", Pattern.compile("^&|and|AND")),
    OR("OR", Pattern.compile("^\\||or|OR")),
    NOT("NOT", Pattern.compile("^!")),

    TRUE("TRUE", Pattern.compile("^true|TRUE")),
    FALSE("FALSE", Pattern.compile("^false|FALSE")),

    NUMBER("NUMBER", Pattern.compile("^0|[1-9][0-9]*")),
    ID("ID", Pattern.compile("^[A-Za-z_]+")),

    SEMICOLON("SEMICOLON", Pattern.compile("^;")),

    LEFT_ROUND_BRACKET("LEFT_ROUND_BRACKET", Pattern.compile("^\\(")),
    RIGHT_ROUND_BRACKET("RIGHT_ROUND_BRACKET", Pattern.compile("^\\)")),

    LEFT_CURLY_BRACKET("LEFT_CURLY_BRACKET", Pattern.compile("^\\{")),
    RIGHT_CURLY_BRACKET("RIGHT_CURLY_BRACKET", Pattern.compile("^}")),

    UNCONDITIONAL_TRANSITION("UNCONDITIONAL_TRANSITION", Pattern.compile("^$")),

    TRUE_TRANSITION("TRUE_TRANSITION", Pattern.compile("^$")),
    FALSE_TRANSITION("FALSE_TRANSITION", Pattern.compile("^$"));

    private String name;
    private Pattern pattern;

    Lexeme(String name, Pattern pattern) {
        this.name = name;
        this.pattern = pattern;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getName() {
        return name;
    }
}
