public final class Token {

    private Lexeme lexeme;
    private String value;

    Token(String string) {
        for (Lexeme lexeme : Lexeme.values())
            if (string.matches(lexeme.getPattern().toString())) {
                this.lexeme = lexeme;
                this.value = string;
                break;
            }
    }

    Token(Lexeme lexeme, String value) {
        this.lexeme = lexeme;
        this.value = value;
    }

    Lexeme getLexeme() {
        return lexeme;
    }

    public void setLexeme(Lexeme lexeme) {
        this.lexeme = lexeme;
    }

    String getValue() {
        return value;
    }

    void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        if (lexeme.equals(Lexeme.NUMBER) ||
                lexeme.equals(Lexeme.ID) ||
                lexeme.equals(Lexeme.FALSE_TRANSITION) ||
                lexeme.equals(Lexeme.TRUE_TRANSITION) ||
                lexeme.equals(Lexeme.UNCONDITIONAL_TRANSITION))
            return "[" + lexeme.getName() + " : \'" + value + "\']";
        else
            return "[" + lexeme.getName() + "]";
    }
}